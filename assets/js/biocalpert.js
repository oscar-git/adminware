/**
 * Created by oscar on 5/04/15.
 */
var app = angular.module( "BiocalperModule", ['ngRoute',
                                              'DashboardModule',
                                              'ProveedorModule',
                                              'ProductoModule',
                                              'EntradaModule',
                                              'SalidaModule',
                                              'AlmacenModule',
                                              'ServicioModule',
                                              'PrestamoModule',
                                              'angular-confirm',
                                              'ClienteModule'] );
app.controller( "BiocalperController", function($scope, $http, $rootScope, $location) {
  $rootScope.usuario = webUtil.getJSON("usuario");
  $rootScope.modulo = "";
  $rootScope.countProveedores = 0;
  $rootScope.countClientes = 0;

  console.log("usuario :: ", $rootScope.usuario);

  // Recuperando los proveedores disponibles
  $http.get("/Proveedor/findAllBasic/").then( function(result){
    console.log( JSON.stringify(result.data) );
    if( result.data.length > 0  ){
      $rootScope.proveedoresMenu = result.data;
      $rootScope.countProveedores = result.data.length;
    }else{
      $rootScope.proveedoresMenu = [{id:-1,nombre:"No tienes proveedores registrados"}];
    }
  });

  // Recuperando el Count de Clientes
  $http.get("/Cliente/count/").then(function(result){
    //console.log("Result Count Clientes:", result);
    $rootScope.countClientes = result.data;
  });

  $scope.logout = function( ){
    $http.post("/logout").then(function(result){
      console.log("Logout exito:: ", result );
      window.location.href = "/";
    }, function(err){
      console.log("Error al intentar logout", err);
    });
  }


});

app.config(function( $routeProvider, $locationProvider){
  //$routeProvider.when('/', {templateUrl: 'pages/dashboard.html'});
  $routeProvider.when('/productos/:proveedorId',   {templateUrl: 'pages/productos.html'});
  $routeProvider.when('/proveedores',  {templateUrl: 'pages/proveedor.html'});
  $routeProvider.when('/entradas',  {templateUrl: 'pages/entradas.html'});
  $routeProvider.when('/salidas',  {templateUrl: 'pages/salidas.html'});
  $routeProvider.when('/almacen',  {templateUrl: 'pages/almacen.html'});
  $routeProvider.when('/clientes',   {templateUrl: 'pages/cliente.html'});
  $routeProvider.when('/servicios',   {templateUrl: 'pages/servicios.html'});
  $routeProvider.when('/prestamos',   {templateUrl: 'pages/prestamos.html'});
  $routeProvider.when('/usuarios',   {templateUrl: 'pages/usuario.html'});

  //$confirmModalDefaults.templateUrl = 'myModalContent.html';
  //$confirmModalDefaults.defaultLabels.title = 'Modal Title';
  //$confirmModalDefaults.defaultLabels.ok = 'Yes';
  //$confirmModalDefaults.defaultLabels.cancel = 'No';
  //localStorage.clear();
});

app.directive('onlyDigits', function () {
  return {
    require: 'ngModel',
    restrict: 'A',
    link: function (scope, element, attr, ctrl) {
      function inputValue(val) {
        if (val) {
          var digits = val.replace(/[^0-9]/g, '');

          if (digits !== val) {
            ctrl.$setViewValue(digits);
            ctrl.$render();
          }
          return parseInt(digits,10);
        }
        return undefined;
      }
      ctrl.$parsers.push(inputValue);
    }
  };
});

app.directive('ngEnter', function() {
  return function(scope, element, attrs) {
    element.bind("keydown keypress", function(event) {
      if(event.which === 13) {
        scope.$apply(function(){
          scope.$eval(attrs.ngEnter, {'event': event});
        });

        event.preventDefault();
      }
    });
  };
});

app.directive('validNumberFloat', function() {
  return {
    require: '?ngModel',
    link: function(scope, element, attrs, ngModelCtrl) {
      if(!ngModelCtrl) {
        return;
      }
      ngModelCtrl.$parsers.push(function(val) {
        if (angular.isUndefined(val)) {
          var val = '';
        }
        var clean = val.replace(/[^0-9\.]/g, '');
        var decimalCheck = clean.split('.');

        if(!angular.isUndefined(decimalCheck[1])) {
          decimalCheck[1] = decimalCheck[1].slice(0,2);
          clean =decimalCheck[0] + '.' + decimalCheck[1];
        }

        if (val !== clean) {
          ngModelCtrl.$setViewValue(clean);
          ngModelCtrl.$render();
        }
        return clean;
      });

      element.bind('keypress', function(event) {
        if(event.keyCode === 32) {
          event.preventDefault();
        }
      });
    }
  };
});
