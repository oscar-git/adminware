/**
 * Created by oscar on 5/04/15.
 */
var app = angular.module( "LoginModule", [] );

app.controller( "LoginController", function($scope, $http, $rootScope) {

  $scope.login = function(){
    $http.post("/login", $scope.usuario).then(function(result){
      console.log( "Result :: " + JSON.stringify(result) );
      if( result.data.message === Constants.LOGIN_SUCCESS ) {
        $http.post("/Usuario/findById/", result.data.user).then( function(resultUser){
          webUtil.save("usuario", resultUser.data);
          window.location.href = "inicio.html";
        });
      } else {
        $scope.errorLogin = true;
      }
    },function(err){
      console.error("Error al login:: " + JSON.stringify(err) );
      $scope.errorLogin = true;
    });
  }


});
