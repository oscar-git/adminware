var myApp = angular.module("ProductoModule",[]);
myApp.controller( "ProductoController", function($scope, $http, $rootScope, $routeParams) {

  $scope.countProductos = 0;
  $scope.producto = {}
  $scope.producto.haveSerie = true;
  if ($routeParams.proveedorId != null) {
    $http.post("/Proveedor/findById/", {id:$routeParams.proveedorId}).then(function(result){
      $scope.proveedor = result.data;
      $scope.countProductos = $scope.proveedor.productos.length;
      //console.log( JSON.stringify(result.data) );
      $rootScope.modulo = "Mis Productos";
    },function(err){
      console.error("Error al obtener el proveedor por id :: " + JSON.stringify(err) )
    });
  }

  $scope.setAsNuevoProducto = function(isNew, index){
    $scope.productoIndex = index;
    $scope.isNuevoProducto = isNew;
    if(isNew) {
      $scope.producto = null;
      $scope.titulo = "Nuevo Producto";
    }else{
      $scope.titulo = "Editar Producto";
      //$('input:radio[name="inputPrioridad"]').filter('[value="1"]').attr('checked', true);
      var pr = $scope.proveedor.productos[index].prioridad;
      $('input:radio[name=inputPrioridad][value=' + pr + ' ]').click();
      $scope.producto = angular.copy($scope.proveedor.productos[index]);
    }
  }

  $scope.accionProducto = function(){
    var valRadio = $("input:radio:checked").val();
    if( valRadio ){
      $scope.producto.prioridad = valRadio;
    }else{
      $scope.producto.prioridad = 5;
    }
    $scope.producto.proveedor = $scope.proveedor.id;
    //console.log( JSON.stringify($scope.producto) );
    if( $scope.isNuevoProducto ) {
      $http.post("/Producto/create/", $scope.producto).then(function (result) {
        $scope.proveedor.productos.push(result.data);
        $scope.countProductos =$scope.proveedor.productos.length;
        console.log( "Productos disponibles :: [" + $scope.countProductos + "] " +
        JSON.stringify($scope.proveedor)  );
        $('#productoModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
    }else{
      $http.post("/Producto/update/", $scope.producto).then(function (result) {
        $scope.proveedor.productos[$scope.productoIndex] = result.data[0];
        $scope.form.$setPristine(true);
        $('#productoModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
    }
  }

  $scope.borrarProducto = function(){
    $http.post("/Producto/update/",
      {id:$scope.proveedor.productos[$scope.productoIndex].id,status:Constants.BORRADO})
      .then(function (result) {
        $scope.proveedor.productos.splice($scope.productoIndex, 1);
        $rootScope.countProductos = $scope.proveedor.productos.length;
        $('#borrarModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
  }




});
