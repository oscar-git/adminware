var myApp = angular.module("EntradaModule",['angular-confirm']);
myApp.controller( "EntradaController", function($scope, $http, $rootScope, $routeParams, $confirm) {

  $rootScope.modulo = "Entradas";
  $scope.entradasCount = 0;

  $scope.$watch('producto', function(productoView) {
    if( productoView && productoView.id === 0 ){
      $scope.formEntrada.producto.$setValidity('required', false);
    }else{
      $scope.formEntrada.producto.$setValidity('required', true);
    }
  });

  $scope.cargaInicial = function(){
    //Cargando Proveedores
    $http.get("/Proveedor/findAll/").then(function(result){
      $scope.proveedores = result.data;
      $scope.proveedor = $scope.proveedores[0];
      $scope.productos = $scope.proveedor.productos;
      if( $scope.productos.length <= 0 ){
        $scope.productos.push({nombre:"Sin productos asociados", id:0});
      }
      $scope.producto = $scope.productos[0];
      //$scope.productos.push({nombre:"Agregar un producto...", id:-1});
    },function(err){
      console.error(JSON.stringify(err));
    });

    //Cargando Entradas
    $http.get("/Entrada/findAllByDate/").then(function(result){
      //console.log( JSON.stringify(result.data) );
      $scope.entradas = result.data;
      $scope.entradasCount = $scope.entradas.length;

      $('#tablaEntradas').dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
      });

    });

  }

  $scope.getProductosProveedor = function(){
    $scope.productos = $scope.proveedor.productos;
    var tamProd = $scope.productos.length;
    if( tamProd <= 0 ){
      $scope.productos.push({nombre:"Sin productos asociados", id:0});
    }
    $scope.producto = $scope.productos[0];
    if( !$scope.productos[tamProd - 1] || $scope.productos[tamProd - 1].id !== -1  ) {
      //$scope.productos.push({nombre: "Agregar un producto...", id: -1});
    }
  }

  $scope.toAgregarProducto = function(){
    if( $scope.producto.id === -1){
      $('#productoModal').modal('show');
    }
  }

  $scope.clear = function(){
    $scope.mensajeExito = null;
    $scope.mensajeError = null;
  }

  $scope.clearEntrada = function(){
    $scope.entrada.serie = null;
    $scope.entrada.cantidad = null;
    $scope.entrada.noImportacion = null;
    $scope.entrada.numeroAlbaran = null;
  }

  $scope.agregarEntrada = function(){
    if(!$scope.entrada.serie && !$scope.entrada.cantidad){
      $scope.mensajeError = "Debe de proporcionar una Serie o una Cantidad";
      return;
    }
     $confirm({text: 'Favor de confirmar su entrada'})
      .then(function() {
        $scope.clear();
        $scope.entrada.usuario = $rootScope.usuario.id;
        $scope.entrada.producto = $scope.producto.id;
        $scope.entrada.nombreProducto = $scope.producto.nombre;
        $http.post("/Entrada/create/", $scope.entrada).then(function(result){
          //console.log("Entrada registrada :: " + JSON.stringify(result.data) );
          $scope.mensajeExito = "Entrada registrada con exito al sistema";
          $scope.clearEntrada();
          result.data.usuario = $rootScope.usuario;
          result.data.producto = angular.copy($scope.producto);
          if($scope.entradas && $scope.entradas.length > 0) {
            $scope.entradas.unshift(result.data);
          }else{
            $scope.entradas = [result.data];
          }
          $scope.entradasCount = $scope.entradas.length;
        });
    });
  }

  $scope.updateEntrada = function(){
    $http.post("/Entrada/update/", $scope.entradaTemp).then(function(result){
      //console.log("Entrada registrada :: " + JSON.stringify(result.data) );
      $scope.mensajeExito = "Entrada actualizada con exito al sistema";
      $scope.entradas[$scope.entradaIndex] = $scope.entradaTemp ;
      $("#entradaModal").modal("hide");      
    });
  }

  $scope.setEntradaMod = function(index){
    $scope.entradaIndex = index;
    $scope.entradaTemp = angular.copy($scope.entradas[index]);
  }

  $scope.generarPdf = function(index){
      var doc = new jsPDF('p', 'pt', 'letter');
      doc.setFontSize(12);
      doc.setFontType("bold");
      doc.text(10, 50, "Fecha: ");

      doc.setFontType("normal");
      doc.text(50, 50,  $scope.entradas[index].createdAt.substring(0, $scope.entradas[index].createdAt.indexOf("T")));

      doc.setFontSize(21);
      doc.text(200, 80, " ENTRADA AL ALMACEN ");

      doc.setFontSize(12);
      doc.setFontType("bold");
      doc.text(10, 120, "No Importación:");
      doc.setFontType("normal");
      doc.text(150, 120, "" + $scope.entradas[index].noImportacion);

      doc.setFontType("bold");
      doc.text(350, 120, "Serie: ");
      doc.setFontType("normal");
      var serieStr = $scope.entradas[index].serie + "";
      serieStr = (serieStr === "undefined") ? "N/A" : serieStr;
      console.log("serieStr: ", serieStr);
      doc.text(450, 120, serieStr );

      doc.setFontType("bold");
      doc.text(10, 160, "Cantidad: ");
      doc.setFontType("normal");
      var cantidadStr = $scope.entradas[index].cantidad + "";
      cantidadStr = (cantidadStr === "undefined") ? "N/A" : cantidadStr;
      console.log("cantidadStr: ", cantidadStr);
      doc.text(150, 160, cantidadStr);

      doc.setFontSize(12);
      doc.setFontType("bold");
      doc.text(350, 160, "No Albaran: ");

      doc.setFontType("normal");
      doc.text(450, 160, "" + $scope.entradas[index].numeroAlbaran);


      // IMPRIMIENDO PRODUCTOS ASOCIADOS

     var columns = [
        {title: "USUARIO", key: "usuario"},
        {title: "PRODUCTO", key: "nombre"},
        //{title: "DESCRIPCION", key: "descripcion"},
        {title: "CODIGO", key: "codigo"}
      ];
      var productos = [];
      var e = {
        usuario: $scope.entradas[index].usuario.nombre,
        nombre: $scope.entradas[index].producto.nombre,
        //descripcion: $scope.entradas[index].producto.descripcion,
        codigo: $scope.entradas[index].producto.codigo
       }
      productos.push(e);
      doc.autoTable(columns, productos , {startY: 200});
      var splitTitle = doc.splitTextToSize("" );
      doc.text(splitTitle, 40, doc.autoTableEndPosY() + 40);
      doc.save("reporte.pdf");

    }

  $scope.cargaInicial();


});
