/**
 * Created by oscar on 5/04/15.
 */
var registro = angular.module( "PrestamoModule", ['validation.match'] );

registro.controller( "PrestamoController", function($scope, $http, $rootScope) {
  $scope.mensajeError = null;
  $rootScope.modulo = "Equipos en Prestamo";
  $scope.prestamoNuevo = {};
  $scope.isNew = true;

  $scope.init = function(){
    $scope.fechaEntrada = new Date();    
    $scope.loadPrestamos();
    $http.get("/Proveedor/findAll/").then(function(result){
      $scope.proveedores = result.data;
      $scope.proveedor = $scope.proveedores[0];
      $scope.productos = $scope.proveedor.productos;
      if( $scope.productos.length <= 0 ){
        $scope.productos.push({nombre:"Sin productos asociados", id:0});
      }
      $scope.prestamoNuevo.equipo = $scope.productos[0];
    },function(err){
      console.error(JSON.stringify(err));
    });
  }

  $scope.loadPrestamos = function(){
    $http.get("/EquipoPrestamo/find/").then(function(result){
      $scope.prestamos = result.data;
      $rootScope.countPrestamos =$scope.prestamos.length;
    },function(err){
      console.error(JSON.stringify(err));
    });
  }

  $scope.setAsNuevoPrestamoEquipo = function( isNew, index ){
    $scope.isNew = isNew;
    if(isNew){
      $scope.prestamoNuevo = {};
      $scope.prestamoNuevo.equipo = $scope.productos[0];
      $("form")[0].reset();
    }else{
      $scope.indexSelected = index;
      $scope.prestamoNuevo = angular.copy( $scope.prestamos[index] );
      for( var i=0; i < $scope.proveedores.length; i++ ){
        if( $scope.proveedores[i].id === $scope.prestamoNuevo.equipo.proveedor ){
          $scope.proveedor = $scope.proveedores[i];
          $scope.productos = $scope.proveedor.productos;
          for( var j =0; j < $scope.productos.length; j++){
            if($scope.productos[j].id === $scope.prestamoNuevo.equipo.id){
              $scope.prestamoNuevo.equipo = $scope.productos[j];
              break;
            }
          }
          break;
        }
      }
      $scope.fechaEstimadaSalidaTemp = $scope.prestamoNuevo.fechaEstimadaSalida;
    }
  }

  $scope.calendar = {openedFI :false,openedFF :false, format:'dd-MM-yyyy'};
  $scope.openFI = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.calendar.openedFI = true;
  };

  $scope.getProductosProveedor = function(){
    $scope.productos = $scope.proveedor.productos;
    var tamProd = $scope.productos.length;
    if( tamProd <= 0 ){
      $scope.productos.push({nombre:"Sin productos asociados", id:0});
    }
    $scope.producto = $scope.productos[0];
  }

  $scope.registrarPrestamo = function(){
    if($scope.isNew){
      $scope.prestamoNuevo.estatus = 1;
      $http.post("/EquipoPrestamo/create/", $scope.prestamoNuevo)
      .then(function(result){
        $scope.loadPrestamos();
        $("#prestamoModal").modal("hide");
      }).catch(function(err){
        $("#prestamoModal").modal("hide");
        console.error("Error al registrar nuevo equipo: ", err);
      });
    }else{
      var valRadio = $("input:radio:checked").val();
      $scope.prestamoNuevo.estatus = eval(valRadio);
      $http.put("/EquipoPrestamo/update/" + $scope.prestamoNuevo.id, $scope.prestamoNuevo)
      .then(function(result){
        $scope.loadPrestamos();
        $("#prestamoModal").modal("hide");
      }).catch(function(err){
        $("#prestamoModal").modal("hide");
        console.error("Error al registrar nuevo prestamo: ", err);
      });
    }
  }

  $scope.borrarEquipoPrestamo = function(){
    $http.delete("/EquipoPerstamo/destroy/" + $scope.prestamos[$scope.indexSelected].id )
      .then(function (result) {
        $scope.loadPrestamos();
        $('#borrarModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
  }

  $scope.init();

  window.onload = function() {
    $(".hide").each(function(){
      $(this).removeClass("hide");
    });
  };

});

