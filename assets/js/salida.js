var myApp = angular.module("SalidaModule",['angular-confirm']);
myApp.controller( "SalidaController", function($scope, $http, $rootScope, $routeParams, $confirm) {

  $rootScope.modulo = "Salidas";
  $scope.salidasCount = 0;
  $scope.placeholder = "número de serie";

  $scope.cambiarTexto = function(){
    var valRadio = $("input:radio:checked").val();
    if( valRadio === "serie" ){
      $scope.placeholder = "número de serie";
    }else{
      $scope.placeholder = "nombre del producto";
    }
  }

  $scope.cargaInicial = function(){
    //Cargando Proveedores
    $http.get("/Cliente/findAll/").then(function(result){
      $scope.clientes = result.data;
      if( $scope.clientes.length <= 0 ){
        $scope.clientes.push({nombre:"Sin clientes registrados", id:0});
      }
      $scope.cliente = $scope.clientes[0];
      $scope.clientes.push({nombre:"Agregar un cliente...", id:-1});
    },function(err){
      console.error(JSON.stringify(err));
    });

    //Cargando las salidas
    $http.get("/Salida/findAllByDate/").then(function(result) {
      console.log( JSON.stringify(result.data) );
      $scope.salidas = result.data;
      $scope.salidasCount = $scope.salidas.length;

    });

  }

  $scope.buscarEnAlmacen = function(){
    if( $scope.criteria.length > 3 ){
      $scope.productoSelected = null;
      $scope.indexProductoSelected = 0;
      if( $scope.salida ) $scope.salida.precio = null;

      var valRadio = $("input:radio:checked").val();
      if( valRadio === "serie" ){
        //console.log("Buscando por serie del producto");
        $http.post("/Almacen/findBySerie/",{serie:$scope.criteria}).then( function(result){
          $scope.productosInAlmacen = result.data;
        });
      }else{
        //console.log("Buscando por nombre de producto");
        $http.post("/Almacen/findByProductName/",{nombre:$scope.criteria}).then( function(result){
          //console.log( JSON.stringify(result) );
          $scope.productosInAlmacen = result.data;
        });
      }
    }
  }

  $scope.setProductoInAlmacen = function( index ){
    if( index > 0 ){
      $confirm({text: 'Hay un producto en almacen mas antiguo. ¿Desea continuar con el producto seleccionado?'})
      .then(function() {
        $scope.indexProductoSelected = index;
        $scope.productoSelected = $scope.productosInAlmacen[index];    
      });
    }else{
      $scope.indexProductoSelected = index;
      $scope.productoSelected = $scope.productosInAlmacen[index];  
    }
  }

  $scope.agregarSalida = function(){
    $scope.salida.producto =  $scope.productoSelected.producto;
    $scope.salida.nombreProducto =  $scope.productoSelected.nombreProducto;
    $scope.salida.serie =  $scope.productoSelected.serie;
    $scope.salida.cliente =  $scope.cliente.id;
    $scope.salida.usuario =  $rootScope.usuario.id;
    $scope.salida.noImportacion =  $scope.productoSelected.noImportacion;
    $scope.salida.numeroAlbaran =  $scope.productoSelected.numeroAlbaran;
    console.log("salida a crear: ", $scope.salida);
    var request = {idAlmacen: $scope.productoSelected.id, salida: $scope.salida };
    $http.post("/Salida/create/", request).then( function(result){
      $scope.productosInAlmacen.splice($scope.indexProductoSelected, 1);
      $scope.salida = null;
      $scope.mensajeExito = "La salida se ha registrado en el sistema con éxito";
      $scope.cargaInicial();
    });

  }

  // Para crear un nuevo cliente
  $scope.nuevoCliente = function( cliente ) {
    if( cliente.id === -1 ){
      $scope.clienteNuevo = null;
      $('#clienteModal').modal('show');
    }
  }

  $scope.accionCliente = function(){
    $http.post("/Cliente/create/", $scope.clienteNuevo).then(function (result) {
      $rootScope.countClientes =$scope.clientes.length;
      var tamClientes = $scope.clientes.length;
      $scope.clientes[tamClientes] = $scope.clientes[tamClientes - 1];
      $scope.clientes[tamClientes - 1] = result.data;
      tamClientes = $scope.clientes.length;
      $scope.cliente = $scope.clientes[tamClientes - 2] ;
      $scope.clienteNuevo = null;
      $('#clienteModal').modal('hide');
    }, function (err) {
      console.error(JSON.stringify(err));
    });
  }

  $scope.generarSalidaPdf = function(index){
      var doc = new jsPDF('p', 'pt', 'letter');
      doc.setFontSize(12);
      doc.setFontType("bold");
      doc.text(10, 50, "Fecha: ");

      doc.setFontType("normal");
      doc.text(50, 50,  $scope.salidas[index].createdAt.substring(0, $scope.salidas[index].createdAt.indexOf("T")));

      doc.setFontSize(21);
      doc.text(200, 80, " SALIDA DEL ALMACEN ");

      doc.setFontSize(12);
      doc.setFontType("bold");
      doc.text(10, 120, "No Importación:");
      doc.setFontType("normal");
      doc.text(150, 120, "" + $scope.salidas[index].noImportacion);

      doc.setFontType("bold");
      doc.text(350, 120, "Serie: ");
      doc.setFontType("normal");
      var serieStr = $scope.salidas[index].serie + "";
      serieStr = (serieStr === "undefined") ? "N/A" : serieStr;
      console.log("serieStr: ", serieStr);
      doc.text(450, 120, serieStr );

      doc.setFontType("bold");
      doc.text(10, 160, "Cantidad: ");
      doc.setFontType("normal");
      var cantidadStr = $scope.salidas[index].cantidad + "";
      cantidadStr = (cantidadStr === "undefined") ? "N/A" : cantidadStr;
      console.log("cantidadStr: ", cantidadStr);
      doc.text(150, 160, cantidadStr);

      doc.setFontSize(12);
      doc.setFontType("bold");
      doc.text(350, 160, "No Albaran: ");

      doc.setFontType("normal");
      doc.text(450, 160, "" + $scope.salidas[index].numeroAlbaran);


      // IMPRIMIENDO PRODUCTOS ASOCIADOS
      console.log("salida a imprimir: ",$scope.salidas[index] );
     var columns = [
        {title: "USUARIO", key: "usuario"},
        {title: "PRODUCTO", key: "nombre"},
        //{title: "DESCRIPCION", key: "descripcion"},
        {title: "CLIENTE", key: "cliente"}
      ];
      var productos = [];
      var cliente = null;
      for( var i =0; i < $scope.clientes.length; i++ ){
        if( $scope.clientes[i].id === $scope.salidas[index].cliente ){
          cliente = $scope.clientes[i];
          break;
        }
      }
      var e = {
        usuario: $scope.salidas[index].usuario.nombre,
        nombre: $scope.salidas[index].nombreProducto,
        cliente: cliente.nombre,
        codigo: $scope.salidas[index].producto.codigo
       }
      productos.push(e);
      doc.autoTable(columns, productos , {startY: 200});
      var splitTitle = doc.splitTextToSize("" );
      doc.text(splitTitle, 40, doc.autoTableEndPosY() + 40);
      doc.save("reporte_salida.pdf");

    }

  $scope.cargaInicial();


});
