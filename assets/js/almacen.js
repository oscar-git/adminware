var myApp = angular.module("AlmacenModule",[]);
myApp.controller( "AlmacenController", function($scope, $http, $rootScope, $routeParams) {

  $rootScope.modulo = "Almacen";
  $scope.almacenesCount = 0;

  $scope.cargaInicial = function(){
    //Cargando Proveedores
    $http.get("/Cliente/findAll/").then(function(result){
      $scope.clientes = result.data;
      if( $scope.clientes.length <= 0 ){
        $scope.clientes.push({nombre:"Sin clientes registrados", id:0});
      }
      $scope.cliente = $scope.clientes[0];
    },function(err){
      console.error(JSON.stringify(err));
    });

    //Cargando las salidas
    $http.get("/Almacen/findLimit/").then(function(result) {
      //console.log( JSON.stringify(result.data) );
      $scope.almacenes = result.data;
      $scope.almacenesCount = $scope.almacenes.length;
    });

  }

  $scope.busquedaAvanzada = function(){
    $http.post("/Almacen/findAdvanced/", { nombre: $scope.criteria,
                                           fechaInicio:$scope.fechaInicial,
                                           fechaFin:$scope.fechaFinal })
      .then(function(result){
        $scope.almacenes = result.data;
        $scope.almacenesCount = $scope.almacenes.length;
      });
  }

  $scope.setProducto = function( index, producto ){
    $scope.producto = producto;
    $scope.indexAlmacen = index;
  }

  $scope.borrarDeAlmacen = function( ){
    console.log("Producto a borrar: ", $scope.producto);
    console.log("index a borrar: ", $scope.indexAlmacen);
    $http.post("/Almacen/borrar/", $scope.producto ).then(function(result){
      console.log(" exito al borrar de almacen ", result);
      $scope.almacenes.splice( $scope.indexAlmacen, 1 );
      $('#borrarModal').modal('hide');
    },function(error){
      console.log("Error al borrar almacen", error)
    });
  }

  $scope.generarSalida = function(){
    var salida =  angular.copy( $scope.producto ) ;
    //$scope.salida.cliente =  $scope.cliente.id;
    if(!$rootScope.usuario){
      $rootScope.usuario = webUtil.getJSON("usuario");
    }
    salida.usuario =  $rootScope.usuario.id;
    salida.precio = $scope.precio;
    salida.cliente = $scope.cliente.id
    var request = {idAlmacen: $scope.producto.id, salida: salida };
    console.log( "Generar salida para: ",  request);
    delete request.salida.id;
    delete request.salida.createdAt;
    delete request.salida.updatedAt;
    $http.post("/Salida/create/", request).then( function(result){
      $scope.almacenes.splice($scope.indexProductoSelected, 1);
      $scope.salida = null;
      $scope.mensajeExito = "La salida se ha registrado en el sistema con éxito";
      $("#generaSalidaModal").modal("hide");
    });
  }

  $('#finicial').daterangepicker({format: 'DD-MM-YYYY', locale: {
    applyLabel: 'Aplicar',
    cancelLabel: 'Cancelar',
    fromLabel: 'De',
    toLabel: 'a',
    daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi','Sa'],
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
                 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    firstDay: 1
  }}, function(start, end, label) {
    $scope.fechaInicial = start;
    $scope.fechaFinal = end;
    //console.log(start.toISOString(), end.toISOString(), label);
    //console.log( start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  });



  $scope.cargaInicial();


});
