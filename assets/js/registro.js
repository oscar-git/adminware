/**
 * Created by oscar on 5/04/15.
 */
var registro = angular.module( "RegistroModule", ['validation.match'] );

registro.controller( "RegistroController", function($scope, $http, $rootScope) {
  $scope.mensajeError = null;
  $scope.registrar = function(isValid) {
    if( isValid ){
      $http.post("/usuario/nuevo", $scope.usuario).then(
        function(result){
          //console.log("Result :: " + JSON.stringify(result) );
          webUtil.save("usuario", result.data);
          window.location.href = "inicio.html";
        },function(err){
          //console.log("Error :: " + JSON.stringify(err)  );
          $scope.mensajeError = err.data.mensaje;
          //console.log("Error Mensaje :: " +  $scope.mensajeError  );
        }
    );
    }else{
      alert("El form es invalido");
    }
  }
  window.onload = function() {
    $(".hide").each(function(){
      $(this).removeClass("hide");
    });
  };

});

