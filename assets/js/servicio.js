/**
 * Created by oscar on 5/04/15.
 */
var registro = angular.module( "ServicioModule", ['validation.match'] );

registro.controller( "ServicioController", function($scope, $http, $rootScope) {
  $scope.mensajeError = null;
  $rootScope.modulo = "Equipos en Servicio";
  $scope.servicioNuevo = {};
  $scope.isNew = true;

  $scope.init = function(){
    $scope.servicioNuevo.fechaEntrada = new Date();    
    $scope.loadEquiposServicio();
    $http.get("/Proveedor/findAll/").then(function(result){
      $scope.proveedores = result.data;
      $scope.proveedor = $scope.proveedores[0];
      $scope.productos = $scope.proveedor.productos;
      if( $scope.productos.length <= 0 ){
        $scope.productos.push({nombre:"Sin productos asociados", id:0});
      }
      $scope.servicioNuevo.equipo = $scope.productos[0];
    },function(err){
      console.error(JSON.stringify(err));
    });
  }

  $scope.loadEquiposServicio = function(){
    $http.get("/EquipoServicio/find/").then(function(result){
      $scope.servicios = result.data;
      $rootScope.countServicios =$scope.servicios.length;
    },function(err){
      console.error(JSON.stringify(err));
    });
  }

  $scope.setAsNuevoServicioEquipo = function( isNew, index ){
    $scope.isNew = isNew;
    if(isNew){
      $scope.servicioNuevo = {};
      $scope.servicioNuevo.equipo = $scope.productos[0];
      $("form")[0].reset();
    }else{
      $scope.indexSelected = index;
      $scope.servicioNuevo = angular.copy( $scope.servicios[index] );
      for( var i=0; i < $scope.proveedores.length; i++ ){
        if( $scope.proveedores[i].id === $scope.servicioNuevo.equipo.proveedor ){
          $scope.proveedor = $scope.proveedores[i];
          $scope.productos = $scope.proveedor.productos;
          for( var j =0; j < $scope.productos.length; j++){
            if($scope.productos[j].id === $scope.servicioNuevo.equipo.id){
              $scope.servicioNuevo.equipo = $scope.productos[j];
              break;
            }
          }
          break;
        }
      }
      $scope.fechaEstimadaSalidaTemp = $scope.servicioNuevo.fechaEstimadaSalida;
    }
  }

  $scope.calendar = {openedFI :false,openedFF :false, format:'dd-MM-yyyy'};
  $scope.openFI = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.calendar.openedFI = true;
  };

  $scope.getProductosProveedor = function(){
    $scope.productos = $scope.proveedor.productos;
    var tamProd = $scope.productos.length;
    if( tamProd <= 0 ){
      $scope.productos.push({nombre:"Sin productos asociados", id:0});
    }
    $scope.producto = $scope.productos[0];
    if( !$scope.productos[tamProd - 1] || $scope.productos[tamProd - 1].id !== -1  ) {
      //$scope.productos.push({nombre: "Agregar un producto...", id: -1});
    }
  }

  $scope.reportarEquipo = function(){
    if($scope.isNew){
      $scope.servicioNuevo.estatus = 1;
      $http.post("/EquipoServicio/create/", $scope.servicioNuevo)
      .then(function(result){
        $scope.loadEquiposServicio();
        $("#servicioModal").modal("hide");
      }).catch(function(err){
        $("#servicioModal").modal("hide");
        console.error("Error al registrar nuevo equipo: ", err);
      });
    }else{
      var valRadio = $("input:radio:checked").val();
      $scope.servicioNuevo.estatus = eval(valRadio);
      $http.put("/EquipoServicio/update/" + $scope.servicioNuevo.id, $scope.servicioNuevo)
      .then(function(result){
        $scope.loadEquiposServicio();
        $("#servicioModal").modal("hide");
      }).catch(function(err){
        $("#servicioModal").modal("hide");
        console.error("Error al registrar nuevo equipo: ", err);
      });
    }
  }

  $scope.borrarServicio = function(){
    $http.delete("/EquipoServicio/destroy/" + $scope.servicios[$scope.indexSelected].id )
      .then(function (result) {
        $scope.loadEquiposServicio();
        $('#borrarModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
  }

  $scope.init();

  window.onload = function() {
    $(".hide").each(function(){
      $(this).removeClass("hide");
    });
  };

});

