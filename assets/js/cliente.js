var myApp = angular.module("ClienteModule",[]);
myApp.controller( "ClienteController", function($scope, $http, $rootScope, $routeParams) {

  $rootScope.modulo = "Mis Clientes";

  $scope.cargaInicial = function(){
    $http.get("/Cliente/findAll/").then(function(result){
      $scope.clientes = result.data;
      $rootScope.countClientes =$scope.clientes.length;
    },function(err){
      console.error(JSON.stringify(err));
    });
  }

  $scope.setAsNuevoCliente = function(isNew, index){
    $scope.clienteIndex = index;
    $scope.isNuevoCliente = isNew;
    if(isNew) {
      $scope.cliente = null;
      $scope.titulo = "Nuevo Cliente";
    }else{
      $scope.titulo = "Editar Cliente";
      $scope.cliente = angular.copy($scope.clientes[index]);
    }
  }

  $scope.accionCliente = function(){
    if( $scope.isNuevoCliente ) {
      $http.post("/Cliente/create/", $scope.cliente).then(function (result) {
        $scope.clientes.push(result.data);
        $rootScope.countClientes =$scope.clientes.length;
        $('#clienteModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
    }else{
      var valRadio = $("input:radio:checked").val();
      $scope.cliente.status = eval(valRadio);
      console.log("Cliente a modificar: ", $scope.cliente);
      $http.post("/Cliente/update/", $scope.cliente).then(function (result) {
        console.log("Result Upd Cliente: ", result);
        $scope.clientes[$scope.clienteIndex] = $scope.cliente;
        $('#clienteModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
    }
  }

  $scope.borrarCliente = function(){
    $http.post("/Cliente/update/", {id:$scope.clientes[$scope.clienteIndex].id,status:Constants.BORRADO})
      .then(function (result) {
        $scope.clientes.splice($scope.clienteIndex, 1);
        $rootScope.countClientes = $scope.clientes.length;
        $('#borrarModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
  }

  $scope.cargaInicial();


});
