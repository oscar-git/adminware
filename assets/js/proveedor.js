var myApp = angular.module("ProveedorModule",[]);
myApp.controller( "ProveedorController", function($scope, $http, $rootScope) {
  $rootScope.modulo = "Mis Proveedores";

  $scope.cargaInicial = function(){
    $http.get("/Proveedor/findAll/").then(function(result){
      $scope.proveedores = result.data;
      $rootScope.countProveedores =$scope.proveedores.length;
    },function(err){
      console.error(JSON.stringify(err));
    });
  }

  $scope.setAsNuevoProveedor = function(isNew, index){
    $scope.proveedorIndex = index;
    $scope.isNuevoProveedor = isNew;
    if(isNew) {
      $scope.proveedor = null;
      $scope.titulo = "Nuevo Proveedor";
    }else{
      $scope.titulo = "Editar Proveedor";
      $scope.proveedor = angular.copy($scope.proveedores[index]);
    }
  }

  $scope.accionProveedor = function(){
    if( $scope.isNuevoProveedor ) {
      $http.post("/Proveedor/create/", $scope.proveedor).then(function (result) {
        result.data.productos = [];
        $scope.proveedores.push(result.data);
        $rootScope.proveedoresMenu.push( {id:result.data.id, nombre:result.data.nombre} );
        $rootScope.countProveedores =$scope.proveedores.length;
        if($rootScope.proveedoresMenu[0].id === -1){
          $rootScope.proveedoresMenu.splice(0,1);
        }
        $('#proveedorModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
    }else{
      $http.post("/Proveedor/update/", $scope.proveedor).then(function (result) {
        $scope.proveedores[$scope.proveedorIndex] = $scope.proveedor;
        $('#proveedorModal').modal('hide');
      }, function (err) {
        console.error(JSON.stringify(err));
      });
    }
  }

  $scope.borrarProveedor = function(){
    var proveedorDelete = $scope.proveedores[$scope.proveedorIndex];
    $http.post("/Proveedor/update/", {id:proveedorDelete.id, status:Constants.BORRADO})
      .then(function (result) {
        $scope.proveedores.splice($scope.proveedorIndex, 1);
        $rootScope.countProveedores =$scope.proveedores.length;
        for( var iProv in $rootScope.proveedoresMenu ){
          var prov = $rootScope.proveedoresMenu[iProv];
          if( prov.id === proveedorDelete.id ){
            $rootScope.proveedoresMenu.splice(iProv,1);
            break;
          }
        }
        $('#borrarModal').modal('hide');
    }, function (err) {
      console.error(JSON.stringify(err));
    });
  }

  $scope.cargaInicial();

});
