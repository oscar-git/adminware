/**
* Proveedor.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    nombre:{
      type:'string',
      required:true
    },
    descripcion:'string',
    correo: 'string',
    telefono: 'string',
    productos:{
      collection:'producto',
      via:'proveedor'
    },
    status:{
      type:'integer',
      defaultsTo: 1
    }
  }
};

