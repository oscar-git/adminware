/**
* Producto.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    nombre:{
      type:'string',
      required:true
    },
    codigo:'string',
    //precio:'float',
    proveedor:{
      model:'proveedor',
      via:'productos'
    },
    status:{
      type:'integer',
      defaultsTo: 1
    },
    descripcion:'string',
    haveSerie:'boolean',
    prioridad:'integer'  // 1-bajo, 5-medio, 10-alto
  }
};

