/**
* Usuario.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var bcrypt = require('bcrypt-nodejs');
module.exports = {

  attributes: {
    nombre: {
      type:'string',
      required: true
    },
    username: {
      type:'string',
      required: true
    },
    password:{
      type:'string',
      required: true
    },

    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  },

  beforeCreate: function(user, cb) {
    bcrypt.genSalt(10, function(err, salt) {
      //console.log(" ** finished genSalt ** ", salt );
      //console.log(" ** finished genSalt ** ", user );
      bcrypt.hash(user.password, salt, null, function(err, hash) {
        //console.log(" ** finished hash ** ");
        if (err) {
          console.log(err);
          cb(err);
        }else{
          user.password = hash;
          //console.log("sin error, callback cb(null,user) :: ", user );
          cb(null, user);
        }
      });
    });
  }

};

