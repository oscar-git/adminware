/**
* EquipoPrestamo.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	equipo:{
  		model:'producto'
  	},
  	fechaEstimadaDevolucion:'date',
  	nombreRepresentante:'string',
  	condicionesEquipo:'string',
  	estatus:'integer' // 1-en servicio, 2-entregado
  }
};

