/**
* Entrada.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    producto:{
      model:'producto'
    },
    nombreProducto:'string',
    usuario:{
      model:'usuario'
    },
    serie:'string',
    cantidad:'integer',
    noImportacion:'string',
    numeroAlbaran:'integer'
  }
};

