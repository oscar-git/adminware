/**
 * SalidaController
 *
 * @description :: Server-side logic for managing Salidas
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  findAllByDate: function( request, response ){
    var data = request.allParams();
    var fechaInicio, fechaFin;
    //console.log("data ==> ", data);

    if(!data.fechaInicio){
      fechaInicio = new Date();
      fechaInicio.setHours(0);
      fechaInicio.setMinutes(0);
      fechaInicio.setSeconds(0);
      fechaFin = new Date();
      fechaFin.setHours(23);
      fechaFin.setMinutes(59);
      fechaFin.setSeconds(59);
    }

    Salida.find({createdAt:{"$gte": fechaInicio, "$lt": fechaFin}})
      .populate('usuario').sort({createdAt:-1}).then(function(result){
        return response.json( result );
      }).fail(function(err){
        console.error("Error al recuperar todas las salidas: " + JSON.stringify(err) );
        return response.json( 500, err );
      });
  },

  create: function( request, response ){
    var data = request.allParams();
    console.log("Salida:: ", data.salida);
    //console.log("IdAlmacen:: ", data.idAlmacen);
    console.log("Salida producto:: ", data.salida.producto);


    Salida.create(data.salida).then(function(result){
      if(data.salida.cantidad){
        Almacen.findOne({id:data.idAlmacen}).then(function(resultAlmacen){
          console.log("almacen encontrado: ", resultAlmacen);
            //Quitando la cantidad del almacen
          if( eval(data.salida.cantidad) > eval(resultAlmacen.cantidad) ){
            return response.json(500, {message:"La cantidad a salir es mayor a la disponible en Almacen"});
          }
          resultAlmacen.cantidad = resultAlmacen.cantidad - data.salida.cantidad;
          resultAlmacen.save(function(err,s){});
          return response.json( result );
        }).catch(function(e){
          console.error("Error al borrar almacen, borrando la salida: " + JSON.stringify(e) );
          Salida.destroy(result).then( function(e){});
          return response.json( 500, e );
        });
      }else{
        Almacen.destroy({id:data.idAlmacen}).then(function(resultAlmacen){
          console.log("Creado la salida y el registro en almacen borrado ... OK");
          return response.json( result );
        }).fail(function(){
          console.error("Error al borrar almacen, borrando la salida: " + JSON.stringify(err) );
          Salida.destroy(result).then( function(e){});
          return response.json( 500, err );
        });
      }
    }).fail(function(err){
      console.error("Error al crear una salida: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  }

};

