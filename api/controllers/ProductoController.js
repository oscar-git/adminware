/**
 * ProductoController
 *
 * @description :: Server-side logic for managing Productoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  update: function( request, response ){
    var data = request.allParams();
    Producto.update({id:data.id}, data).then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al modificar productos: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  }

};

