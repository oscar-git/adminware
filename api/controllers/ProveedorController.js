/**
 * ProveedorController
 *
 * @description :: Server-side logic for managing Proveedors
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  findAll: function(request, response){
    Proveedor.find({status:StatusService.ACTIVO}).populate('productos',{status:StatusService.ACTIVO})
   .then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al recuperar los proveedores: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },

  findById: function(request, response){
    var data = request.allParams().id;
    Proveedor.findOne({id:data, status:StatusService.ACTIVO}).populate('productos',{status:StatusService.ACTIVO})
      .then(function(result){
        return response.json( result );
      }).fail(function(err){
        console.error("Error al recuperar los proveedores: " + JSON.stringify(err) );
        return response.json( 500, err );
      });
  },

  findAllBasic: function(request, response){
    Proveedor.find({status:StatusService.ACTIVO},{id:1, nombre:1}).then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al recuperar los proveedores: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },



  update: function( request, response ){
    var data = request.allParams();
    Proveedor.update({id:data.id}, data).then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al modificar los proveedores: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  }

};

