/**
 * UsuarioController
 *
 * @description :: Server-side logic for managing Usuarios
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  registrar : function(req, res) {
    var data = req.allParams();
    console.log("data: " + JSON.stringify(data));
    var error = {mensaje:""};
    Usuario.findOne({username:data.username}).exec(function(err, usuario){
      console.log("Usuario encontrado para la validacion del registro", usuario);
      if(usuario){
        error.mensaje = "Ya existe un usuario registrado con el mismo correo electrónico";
        return res.json(error, 500  );
      }
      console.log("registrando el usuario");
      Usuario.create(data).exec( function(err, usuarioNuevo){
        console.log("Regresando de registro ", usuarioNuevo);
        return res.json(usuarioNuevo);
      });
    });
  },

  findById :function(request, response) {
    var data = request.allParams();
    Usuario.findOne({id:data.id}).then(function(result){
      return response.json(result);
    }).fail(function(err){
      console.error("Error al recuperar el Usuario por ID", err);
      return response.json(500, err);
    });
  }

};

