/**
 * ClienteController
 *
 * @description :: Server-side logic for managing Clientes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  findAll: function(request, response){
    Cliente.find().then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al recuperar los clientes: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },

  update: function( request, response ){
    var data = request.allParams();
    Cliente.update({id:data.id}, data).then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al modificar los clientes: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },

  count: function( request, response ){
    Cliente.count().then(function(result){
      return response.json( result );
    }).fail( function(err){
      console.error("Error al recuperar count de clientes: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  }

};

