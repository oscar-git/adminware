/**
 * AlmacenController
 *
 * @description :: Server-side logic for managing Almacens
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  findBySerie: function(request, response) {
    var data = request.allParams();
    //console.log( "Serie: " + JSON.stringify(data) );
    Almacen.find({ serie: { contains: data.serie } }).populate("producto")
      .then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al recuperar los productos por almacen: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },
  findByProductName: function(request, response) {
    var data = request.allParams();
    //console.log( "Producto nombre: " + JSON.stringify(data) );
    Almacen.find({nombreProducto:{ contains: data.nombre }})
      .then(function(result){
        //console.log( "Almacen: " + JSON.stringify(result) );
        return response.json( result );
      }).fail(function(err){
        console.error("Error al recuperar los productos por almacen: " + JSON.stringify(err) );
        return response.json( 500, err );
      });
  },

  findAllByDate: function( request, response ){
    var data = request.allParams();
    var fechaInicio, fechaFin;
    //console.log("data ==> ", data);
    if(!data.fechaInicio){
      fechaInicio = new Date();
      fechaInicio.setHours(0);
      fechaInicio.setMinutes(0);
      fechaInicio.setSeconds(0);
      fechaFin = new Date();
      fechaFin.setHours(23);
      fechaFin.setMinutes(59);
      fechaFin.setSeconds(59);
    }

    Almacen.find({createdAt:{"$gte": fechaInicio, "$lt": fechaFin}})
      .populate('usuario').sort({createdAt:-1}).then(function(result){
        return response.json( result );
      }).fail(function(err){
        console.error("Error al recuperar todas los productod del almacen: " + JSON.stringify(err) );
        return response.json( 500, err );
      });
  },

  findAdvanced: function( request, response ){
    var data = request.allParams();
    var fechaInicio, fechaFin, nombre;
    if( data.fechaInicio ) fechaInicio = new Date(data.fechaInicio);
    if( data.fechaFin ) {
      fechaFin = new Date(data.fechaFin);
      fechaFin.setHours(23);
      fechaFin.setMinutes(59);
    }
    if( data.nombre ) nombre = data.nombre;



    console.log( fechaInicio + " - " + fechaFin + " ==> " + nombre )
    if( fechaInicio && !nombre ) {
      console.log("Buscando solo por fechas");
      Almacen.find({createdAt: {"$gte": fechaInicio, "$lt": fechaFin}})
        .populate('usuario').sort({createdAt: -1}).then(function (result) {
          return response.json(result);
        }).fail(function (err) {
          console.error("Error al recuperar todas los productos del almacen: " + JSON.stringify(err));
          return response.json(500, err);
        });
    }else if( fechaInicio && nombre ){
      console.log("Buscando por fechas y nombre del producto");
      Almacen.find({createdAt: {"$gte": fechaInicio, "$lt": fechaFin}, nombreProducto:{ contains: data.nombre }})
        .populate('usuario').sort({createdAt: -1}).then(function (result) {
          return response.json(result);
        }).fail(function (err) {
          console.error("Error al recuperar todas los productos del almacen: " + JSON.stringify(err));
          return response.json(500, err);
        });
    }else{
      console.log("Buscando solo por nombre");
      Almacen.find({nombreProducto:{ contains: data.nombre }})
    .populate('usuario').sort({createdAt: -1}).then(function (result) {
        return response.json(result);
      }).fail(function (err) {
        console.error("Error al recuperar todas los productos del almacen: " + JSON.stringify(err));
        return response.json(500, err);
      });
    }
  },

  findLimit: function(request, response){
    var data = request.allParams();
    if( !data.limit ){
      data.limit = 50;
    }
    Almacen.find().populate('usuario').sort({createdAt:-1}).limit( data.limit).then(function(result){
      return response.json( result );
    }).
      fail(function(err){
      console.error("Error al recuperar todas los productod del almacen: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },

  update: function( request, response ){
    var data = request.allParams();
    Almacen.update({id:data.id}, data).then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al modificar el almacen: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },

  borrar: function( request, response ){
    var data = request.allParams();
    console.log("Elemento a borrar: " + JSON.stringify(data) );
    Almacen.destroy({id:data.id} ).then(function(result){
      Entrada.destroy({producto:data.producto, serie:data.serie}).then();
      return response.json( result );
    }).fail(function(err){
      console.error("Error al borrar de almacen: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  }

};

