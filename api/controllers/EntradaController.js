/**
 * EntradaController
 *
 * @description :: Server-side logic for managing Entradas
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  findAll: function( request, response ){
    //var data = request.allParams();
    Entrada.find().populate('usuario').populate('producto').sort({createdAt:-1}).then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al recuperar todas las entradas: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },

  findAllByDate: function( request, response ){
    var data = request.allParams();
    var fechaInicio, fechaFin;
    //console.log("data ==> ", data);
    if(!data.fechaInicio){
      fechaInicio = new Date();
      fechaInicio.setHours(0);
      fechaInicio.setMinutes(0);
      fechaInicio.setSeconds(0);
      fechaFin = new Date();
      fechaFin.setHours(23);
      fechaFin.setMinutes(59);
      fechaFin.setSeconds(59);
    }

    Entrada.find({createdAt:{"$gte": fechaInicio, "$lt": fechaFin}})
           .populate('usuario')
           .populate('producto').sort({createdAt:-1}).then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al recuperar todas las entradas: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },

  update: function( request, response ){
    var data = request.allParams();
    Entrada.update({id:data.id}, data).then(function(result){
      return response.json( result );
    }).fail(function(err){
      console.error("Error al modificar una entrada: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  },

  create: function( request, response ){
    var data = request.allParams();
    Entrada.create(data).then(function(result){

      Almacen.create(data).then(function(resultAlmacen){
        console.log("Creado la entrada y el registro en almacen OK");
        return response.json( result );
      }).fail(function(){
        console.error("Error al crear almacen, borrando la entrada: " + JSON.stringify(err) );
        Entrada.destroy(result).then( function(e){});
        return response.json( 500, err );
      });
    }).fail(function(err){
      console.error("Error al crear una entrada: " + JSON.stringify(err) );
      return response.json( 500, err );
    });
  }

};

